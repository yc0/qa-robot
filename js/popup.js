﻿function sendMessageToContentScript(message, callback){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs)
    {
        chrome.tabs.sendMessage(tabs[0].id, message, function(response)
        {
            if(callback) callback(response);
        });
    });
}

// 點擊重新偵測按鈕後發送消息給content_script
function onClkReDetect(){
	sendMessageToContentScript({cmd:'detect', value:'detect KoHei'}, function(response){
		console.log(response);
		var btnReDetect = document.getElementById('BtnReDetect');
		btnReDetect.classList.remove('active');
	});
}

document.addEventListener('DOMContentLoaded', function() {
    var btnReDetect = document.getElementById('BtnReDetect');

    btnReDetect.addEventListener('click', function() {
		btnReDetect.classList.toggle('active');
		onClkReDetect();
    });
});
