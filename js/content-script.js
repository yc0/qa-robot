﻿var g_aKoHei = new Array();
var g_xmlhttp;
var g_bLoadFromStg = false; // 柯黑名單是否從chrome.storage讀取
var g_bShowReportBtn = false; // 是否顯示檢舉按鈕
var g_bOpt_ShowReportBtn = true; // 內部設定是否允許顯示檢舉按鈕
var g_bIAmNotKoHei = false; // 當前用戶是否不為柯黑(true:不為柯黑;false:可能為柯黑)
var g_strUserUrl = ""; // 當前用戶URL
var g_aMarkStyle; // 標記柯黑的樣式
var g_strOnLoadUrl = window.location.href; // 載入Content Script時的URL
var g_qtext = ""; // 題庫
var g_Count = 0;
/*document.addEventListener('DOMContentLoaded', function()
{
	console.log("content script DOMContentLoaded");
	
	{
		
	}
		
});*/

// Initialize
init();

// Message Listener Against popup.js and background.js
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if(request.cmd === 'detect'){ // From popup.js
		//sendResponse('[ KoHeiDetector ] content_script::Message Listener - Start Redetect KoHei');
		markKoHei();
	}
	else if(request.cmd === 'delay_init'){ // From background.js
		//sendResponse('[ KoHeiDetector ] content_script::Message Listener - Start Delay Init');
		if(document.location.href.indexOf('review') != -1) {
			console.log("Let's find the answer");
			findAnswer();
		}
		loadAnswers();
		if ($('#q1').hasClass('answersaved')) {
			$('input[type=submit]').click();
		}
		// if ($($('input[type=submit]')[1]).val()==='交卷') {
		// 	$($('input[type=submit]')[1]).click();
		// 	setTimeout(function() {
		// 		if ($($('input[type=button]')[0]).val()==='交卷') {
		// 			$($('input[type=button]')[0]).click();
		// 		}
		// 	},1000);
		// }
		// if ($($('input[type=submit]')[0]).val()==='再測驗一次') {
		// 	$($('input[type=submit]')[0]).click();
		// }
		
	}
	else if(request.cmd === 'reload_page'){ // From background.js
		//sendResponse('[ KoHeiDetector ] content_script::Message Listener - Start Reload Page');
		if( g_strOnLoadUrl !== window.location.href ) // 跳轉頁面後的URL與載入時不同才刷新
			location.reload();
		else // 在同一個頁面展開留言後點了回覆按鈕也會觸發chrome.webNavigation.onHistoryStateUpdated，避開刷新頁面
		// 會有跳轉到同一個頁面的問題，但因為手動跳轉到同一個頁面機率不高所以忽略
			init();
	}
});

// Window Message Listener Against inject.js
window.addEventListener("message", function(e){
	if( e.data.cmd === "report" ){
		// Send Report Data To background.js
		chrome.runtime.sendMessage( {cmd:'report', value:e.data.value}, function(response) {
			//console.log(response);
		});
	}
	else if( e.data.cmd === "close_usage_tip" ){
		// 關閉使用提示
		injectUsageTip(false);
	}
	else if( e.data.cmd === "setting_options" ){
		// 開啟偵測器設定選項頁
		var strAppID = chrome.runtime.id;
		var strOptionsUrl = "chrome-extension://" + strAppID + "/options.html";
		window.open( strOptionsUrl );
	}
	else if( e.data.cmd === "automated_answer" ){
		// 開啟偵測器設定選項頁
		doAnwser();
	}
}, false);

// 偵測捲軸到頁面底部
$(window).scroll(function(){
	// Returns height of browser viewport
	var window_height = $( window ).height(); // 當前可見區域的大小
	var window_scrollTop = $(window).scrollTop(); // 獲取垂直滾動的距離 (即當前滾動的地方的窗口頂端到整個頁面頂端的距離)
	// Returns height of HTML document
	var document_height = $( document ).height(); // 整個文檔的高度
	
	if( ( window_height + window_scrollTop ) === document_height ){
		//updatePageContent();
	}
});



function findAnswer() {
	$('div.que').each( function(){
		var qtext = $($(this).find('div.qtext')[0]).html();
		if (qtext) {
			qtext = qtext.replace(/<(?:.|\n)*?>/gm, '');
			var ans = [];
			$(this).find('div.answer').find("input[id*=':']").not('.questionflagvalue').each(function(idx) {
				that = this;
				if($(that).prev().prop('src').indexOf('grade_answer') != -1) {					
					ans.push($(that).next().html().split(" ").slice(1).join(""));
				}
			});
			console.log(qtext, ans);
			handleAnswer(qtext, ans);
		}
	});
}
function doAnwser() {
	g_Count =  $('div.notyetanswered').length;
	$('div.notyetanswered').each(function(idx) {
		var qtext = $($(this).find('div.qtext')[0]).html();
		if (qtext) {
			qtext = qtext.replace(/<(?:.|\n)*?>/gm, '');
			
			// chrome.storage.local.set({[qtext]: "["+qtext+"]"}, function(){

			// });
			// chrome.storage.onChanged.addListener(function(changes,areaName) {
			// 	console.log(changes);
			// 	chrome.storage.local.get(qtext, function(data){
			// 		console.log(data[qtext]);				
			// 	});
			// });
			handleQuestion(this, idx, qtext);
		}
		
		
	});

	
}
function handleAnswer(qtext, ans) {
	chrome.storage.local.get(qtext, function(data) {
		rst = data[qtext];
		
		console.log(qtext);
		if(rst) {
			if (!rst.contains(ans)) {
				rst = rst.concat(ans);
				chrome.storage.local.set({[qtext]: rst}, function(){});
				console.log("rst:",rst);
			}
		} else {
			chrome.storage.local.set({[qtext]: ans}, function(){
				console.log('Value is set to ' + ans);
			});
			chrome.storage.local.get(qtext, function(data){
				console.log(qtext);
				console.log(data);
				console.log(data[qtext]);
			});
		}
	});
}

function handleQuestion(me, idx, qtext) {
	chrome.storage.local.get(qtext, function(data){
		g_Count--;	
		if (data[qtext]) {
			isAnswered = false;			
			$(me).find("input[id*=':']").not('.questionflagvalue').each(function(idx){
				var anwser = $(this).next().html().split(" ").slice(1).join("");
				if(data[qtext].includes(anwser)) {
					isAnswered = true;
					if($(me).find('div.prompt').html() !== "複選:") {
						$(this).prop('checked',true);
					} else {
						this.checked = true;
					}
				}
			});
			if (isAnswered) {
				return;
			}
		} 
		// guess answer
		var name = $(me).find('input[type=hidden]').attr('name');		
		name = name.split(":")[0];
		if ($(me).hasClass('multichoice')) {
			var choice = $(me).find('input[name="'+name+":"+(idx+1)+'_answer"]');		
			$(choice[0]).prop('checked', true);
		} else if ($(me).hasClass('allchoice')) {
			$(me).find('input[type=checkbox]').each(function(){
				this.checked = true;
			});
		}
		
		// if (!g_Count) {
		// 	$('#responseform').submit();
		// }
	});
	
}
// =====================================================================
// Declare Funtion
// =====================================================================
function outputConsoleLog( strLog ){
	console.log( "[ KoHeiDetector ] content_script::" + strLog );
}

// 設定是否顯示檢舉按鈕
function setReportBtnVisible( bShow ){
	g_bShowReportBtn = bShow;
}

// 檢舉按鈕是否可顯示
function isReportBtnVisible(){
	return ( g_bShowReportBtn && g_bOpt_ShowReportBtn );
}

// 設定當前用戶是否不為柯黑
function setIAmNotKoHei( bNotKoHei ){
	g_bIAmNotKoHei = bNotKoHei;
}

// 當前用戶是否不為柯黑
function isNotKoHei(){
	return g_bIAmNotKoHei;
}

//
function injectCustomMeta(charset){
    /*var temp = document.createElement('meta');
    temp.charset = charset;
    var head = document.getElementsByTagName('head')[0].appendChild(temp);*/
	$('head').append('<meta charset=utf-8>');
	//outputConsoleLog("injectCustomMeta - charset=utf-8");
}

function injectGoogleAnalytics(){
	var _gaq = _gaq || [];
	_gaq.push(["_setAccount", "UA-129006257-1"]), _gaq.push(["_trackPageview"]),
	    function() {
	        var e = document.createElement("script");
	        e.type = "text/javascript", e.async = !0, e.src = "https://ssl.google-analytics.com/ga.js";
	        var t = document.getElementsByTagName("script")[0];
	        t.parentNode.insertBefore(e, t)
	    }(), document.addEventListener("DOMContentLoaded", function() {
	        chrome.tabs.query({
	            active: !0,
	            currentWindow: !0
	        }, function(e) {
				console.log("QA loaded");
				console.log(e);
	        })
		});
		
	var e = document.createElement("script");
	e.type = "text/javascript", e.async = !0, e.src = "https://www.googletagmanager.com/gtag/js?id=UA-129006257-1";
	var t = document.getElementsByTagName("script")[0];
	t.parentNode.insertBefore(e, t);
		
	window.dataLayer = window.dataLayer || [];
	setTimeout(function() {
		gtag('js', new Date());
		gtag('config', 'UA-129006257-1');
	},2000);
}
function gtag(){
	console.log(arguments);
	dataLayer.push(arguments);
}
//
function getStgSync_Option_ShowReportBtn(data){
	g_bOpt_ShowReportBtn = data.khdt_opt_showReportBtn;

	if( typeof( g_bOpt_ShowReportBtn ) === "undefined" ){
		chrome.storage.sync.set({khdt_opt_showReportBtn: true}, function(){});
		g_bOpt_ShowReportBtn = true;
	}
}

// 讀取柯黑名單(目前只從線上讀取)
function loadKoHeiList(){
	if( g_bLoadFromStg ){
		chrome.storage.local.get('arrKoHei', getStorageLocal);
	}
	else{
		/*
		chrome.storage.local.clear(function(){
		});
		chrome.storage.sync.clear(function(){
		});
		*/
		//TODO
		//getPage("https://raw.githubusercontent.com/koheileader/KoHeiDetector/master/koheilist.txt");
	}
}

// Initialize
function init(){
	// Inject Meta Tag
	injectCustomMeta("utf-8");
	injectGoogleAnalytics();
	// 獲取內部設定是否允許顯示檢舉按鈕
	//chrome.storage.sync.get('khdt_opt_showReportBtn', getStgSync_Option_ShowReportBtn);

	// Get Mark KoHei Style
	// chrome.storage.sync.get('khdt_opt_markStyle', function(data){
	// 	g_aMarkStyle = data.khdt_opt_markStyle;
	// 	if( typeof( g_aMarkStyle ) === "undefined" ){
	// 		g_aMarkStyle = [ "#000000", "#ffffff" ];
	// 		chrome.storage.sync.set({khdt_opt_markStyle: g_aMarkStyle}, function(){});
	// 	}
	// });

	// Send Delay Init Command To background.js
	chrome.runtime.sendMessage( {cmd:'delay_init', value:''}, function(response) {
		//console.log(response);
	});
}

// Delay Initialize
function delayInit(){
	// Mouse Click Event Listener
	//injectMouseClkEvent();

	// 讀取紀錄判斷是否向頁面插入使用提示
	//chrome.storage.local.get('showUsageTip', onGetStgShowUsageTip);

	// 向頁面插入"偵測器設定"按鈕
	//injectOptionsPageBtn();

	// 向頁面注入JS
	injectCustomJs("");
	
	// 向頁面插入自訂右鍵選單
	//injectContextMenu();
	
	// 鼠標點擊任意處關閉自訂右鍵選單
	document.onclick = function(e){
		var e = e || window.event;
		var menu = document.getElementById("KHDT_contextmenu");
		if( menu )
			menu.style.display = "none";
	}

	// Load KoHei List
	//loadKoHeiList();
	loadAnsBtn();
}

//
function getStorageLocal(data){
	g_aKoHei = data.arrKoHei;
	
	if(g_aKoHei.length > 0){
		outputConsoleLog("getStorageLocal - chrome.storage.local讀取成功！");
		
		// 設定可顯示檢舉按鈕
		setReportBtnVisible( true );
		
		// 更新頁面顯示
		updatePageContent();
	}
	else{
		// 若讀取不到，則讀取chrome.storage.sync
		chrome.storage.sync.get('arrKoHei', getStorageSync);
	}
}

//
function getStorageSync(data){
	g_aKoHei = data.arrKoHei;

	if(g_aKoHei.length > 0){
		outputConsoleLog("getStorageSync - chrome.storage.sync讀取成功！");
		
		// 若有，則同步到chrome.storage.local
		chrome.storage.local.set({arrKoHei: g_aKoHei}, function(){
			outputConsoleLog("getStorageSync - chrome.storage.local保存成功！");
		});
		
		// 設定可顯示檢舉按鈕
		setReportBtnVisible( true );

		// 更新頁面顯示
		updatePageContent();
	}
	else{
		// 若仍然讀取不到，則從線上更新
		getPage("https://raw.githubusercontent.com/koheileader/KoHeiDetector/master/koheilist.txt");
	}
}

// 創建XMLHttp對象，用於讀取遠程文檔
function createXMLHttp(){
	try {
		return new ActiveXObject ("Microsoft.XMLHTTP");
	}
	catch(e){
		try {
			return new XMLHttpRequest();
		}
		catch(e) {
			return null;
		}
	}
	return null;
}

function loadAnswers() {
	console.log("load answers......");
	chrome.storage.local.get(null, function(items) {
		var allKeys = Object.keys(items);	
		console.log(allKeys);
	});
	$('div.notyetanswered').each(function(idx) {
		var qtext = $($(this).find('div.qtext')[0]).html();
		if (qtext) {
			qtext = qtext.replace(/<(?:.|\n)*?>/gm, '');
			g_qtext = qtext;
			getPage("https://raw.githubusercontent.com/yc0/respondence-4-ilearning/master/"+qtext+".txt");
		}
	});
	
}

function getPage(pageURL) {
	g_xmlhttp = createXMLHttp();
	if(g_xmlhttp)
	{
		g_xmlhttp.onreadystatechange = setPageData;
		g_xmlhttp.open('GET', pageURL);
		// g_xmlhttp.send(null);
	}
	else{
		alert("XMLHttpRequest object is null!");
	}
}

// 回調函數，獲得從服務器回發的文檔信息並儲存到陣列中
function setPageData(){
	if(g_xmlhttp.readyState === 4 && g_xmlhttp.status === 200){
		g_aKoHei = g_xmlhttp.responseText.split("\n");
		
		// 更新完後存儲名單到chrome.storage.local, chrome.storage.sync
		// if( g_bLoadFromStg ){
		// 	chrome.storage.local.set({arrKoHei: g_aKoHei}, function(){
		// 		outputConsoleLog("setPageData - chrome.storage.local保存成功！");
		// 	});
		// 	chrome.storage.sync.set({arrKoHei: g_aKoHei}, function(){
		// 		outputConsoleLog("setPageData - chrome.storage.sync保存成功！");
		// 	});
		// }
		
		// // 設定可顯示檢舉按鈕
		// setReportBtnVisible( true );

		// // 根據柯黑名單改變帳號底色
		// updatePageContent();
	}
	//debugger;
	delayInit();
}

// 檢測當前用戶是否為柯黑
function verifyMyIdentity(){
	if(g_aKoHei.length === 0){
		outputConsoleLog("verifyMyIdentity - Error: KoHei list is empty!");
		return;
	}
	
	var aKoHeiInfo; // 柯黑資訊( format: 帳號名"主頁連結 )
	
	// TODO: 檢測目前登入者是否為柯黑，不讓柯黑進入網頁
	// 檢測 script tag
	var aScripts = document.getElementsByTagName("script");
	var strTmpKoHeiUrl;
	var strTmpUserUrl;
	for( iScript = 0; iScript < aScripts.length; ++iScript ) {
		for( iKoHei = 0 ; iKoHei < g_aKoHei.length ; ++iKoHei ){
			aKoHeiInfo = g_aKoHei[iKoHei].split("\"");
			if( typeof(aKoHeiInfo[1]) !== "undefined" ){
				strTmpKoHeiUrl = aKoHeiInfo[1];
				strTmpKoHeiUrl = strTmpKoHeiUrl.replace('/channel/', '');
				strTmpKoHeiUrl = strTmpKoHeiUrl.replace('/user/', '');
				strTmpKoHeiUrl = "\"key\":\"creator_channel_id\",\"value\":\"" + strTmpKoHeiUrl + "\""
				if( aScripts[iScript].innerHTML.search(strTmpKoHeiUrl) !== -1 ){
					//console.log("script - You are KoHei");
					window.location.replace('https://www.teamkp.tw/');
					return;
				}
			}
		}

		// 抓取當前用戶URL
		strTmpUserUrl = aScripts[iScript].innerHTML;
		var iUserUrlIdx = strTmpUserUrl.indexOf("{\"key\":\"creator_channel_id\",\"value\":\"");
		if( iUserUrlIdx !== -1 ){
			strTmpUserUrl = strTmpUserUrl.substr( ( iUserUrlIdx + 37 ), 24 );
			g_strUserUrl = strTmpUserUrl;
		}
	}

	// 檢測 class tag
	var aClasses = document.getElementsByClassName("yt-simple-endpoint style-scope yt-formatted-string");
	for( iClass = 0; iClass < aClasses.length; ++iClass ){
		for( iKoHei = 0 ; iKoHei < g_aKoHei.length ; ++iKoHei ){
			aKoHeiInfo = g_aKoHei[iKoHei].split("\"");
			if( typeof(aKoHeiInfo[1]) !== "undefined" ){
				if( ( aClasses[iClass].innerHTML === "媒體庫" ) && ( aClasses[iClass].href.search( aKoHeiInfo[1] ) !== -1 ) ){
					//console.log("class - You are KoHei");
					window.location.replace('https://www.teamkp.tw/');
					return;
				}
			}
		}
	}
	// ENDDO: 檢測目前登入者是否為柯黑，不讓柯黑進入網頁
	
	setIAmNotKoHei( true ); // 設定當前用戶不為柯黑
}

// 根據柯黑名單改變帳號底色
function markKoHei(){
	if(g_aKoHei.length === 0){
		outputConsoleLog("markKoHei - Error: KoHei list is empty!");
		return;
	}

	// 持續檢測評論區是否已載入
	var iTimes = 0;
	for( iTimes = 0 ; iTimes < 100 ; ++iTimes ){ // 限制重新檢測100次
		var aElements = document.getElementsByClassName("style-scope ytd-item-section-renderer");
		var idx = 0;
		for( idx = 0; idx < aElements.length; ++idx ){
			if(aElements[idx].id === "contents"){
				//outputConsoleLog("markKoHei - Found comment contents");
				break; // 評論區已載入
			}
		}
		
		if( idx < aElements.length )
			break; // 結束檢測評論區
	}

	// 檢測評論區的Youtube ID
	var bFoundKoHei = false;
	var aElements = document.getElementsByClassName("style-scope ytd-comment-renderer"); // 所有留言帳號的Element
	var aSpans; // 留言帳號Element的所有span
	var strSpanTxt; // span文字內容
	var aKoHeiInfo; // 柯黑資訊( format: 帳號名"主頁連結 )
	var strTmpHref; // Temp string for replacing

	for( iElement = 0; iElement < aElements.length; ++iElement ){
		if(aElements[iElement].id === "author-text"){
			// 移除自訂樣式
			aElements[iElement].classList.remove("markKoHei");
			aElements[iElement].style.backgroundColor = "";

			// 獲取子Span
			aSpans = aElements[iElement].getElementsByTagName("span");

			// 解析帳號連結
			strTmpHref = aElements[iElement].href;
			strTmpHref = strTmpHref.replace(/^.*\/\/[^\/]+/, '');

			for( iSpan = 0 ; iSpan < aSpans.length ; ++iSpan ){
				// 移除自訂樣式
				aSpans[iSpan].classList.remove("markKoHei");
				aSpans[iSpan].style.color = "";

				// 解析帳號名稱
				strSpanTxt = aSpans[iSpan].innerHTML;
				strSpanTxt = strSpanTxt.replace(/^\s*/, '');
				strSpanTxt = strSpanTxt.replace(/\n\s*/, '');
				//outputConsoleLog( "strSpanTxt: '" + strSpanTxt + "'" );
				
				for( iKoHei = 0 ; iKoHei < g_aKoHei.length; ++iKoHei ){
					aKoHeiInfo = g_aKoHei[iKoHei].split("\"");
					
					//if( strSpanTxt.localeCompare( aKoHeiInfo[0] ) === 0 ){
						// 用戶名稱一樣才繼續檢測(X)
						//outputConsoleLog( "'" + strTmpHref + "' compare with '" + aKoHeiInfo[1] + "'" );

						if( ( typeof(aKoHeiInfo[1]) !== "undefined" ) && ( strTmpHref === aKoHeiInfo[1] ) ){
							// 若驗證發現帳號主頁連結存在於柯黑名單中，則標記柯黑
							aElements[iElement].classList.add("markKoHei");
							aElements[iElement].setAttribute( 'style', 'background-color: ' + g_aMarkStyle[0] );
							aSpans[iSpan].classList.add("markKoHei");
							aSpans[iSpan].setAttribute( 'style', 'color: ' + g_aMarkStyle[1] );
							//outputConsoleLog("markKoHei - KoHei:" + strSpanTxt);
							bFoundKoHei = true;
							break;
						}
					//}
				}
				
			}
		}
	}

	/*if( bFoundKoHei )
		outputConsoleLog("markKoHei - Found KoHei");*/
}

// 顯示檢舉按鈕在帳號旁，以及添加右鍵選單
function showReportBtn(){
	var aClasses = document.getElementsByClassName("style-scope ytd-comment-renderer");
	var aSpans;
	var strKoHeiName;
	var strKoHeiUrl;
	var strReportUrl;
	var strCommentUrl;
	var strReportParam;
	
	for( iClass = 0; iClass < aClasses.length; ++iClass ){
		if( aClasses[iClass].id === "header-author" ){
			var child = aClasses[iClass].firstChild;
			var bHasBtn = false; // 此帳號旁是否已存在按鈕
			
			while( child ){
				if( child.id === "BtnReport" )
					bHasBtn = true; // 不新增按鈕
				else if( child.id === "author-text" ){
					if( child.classList.contains("markKoHei") ){
						bHasBtn = true; // 不新增按鈕
						break;
					}
					else if( child.firstChild !== null ){
						// 解析柯黑帳號連結
						strKoHeiUrl  = child.href;
						strKoHeiUrl  = strKoHeiUrl.replace(/^.*\/\/[^\/]+/, '');
						
						// 若帳號為柯黑偵測器則不顯示按鈕
						if( strKoHeiUrl === "/channel/UCIrO9B2Hizg-iLpEkxynEPw" ){
							bHasBtn = true; // 不新增按鈕
							break;
						}
						
						// 解析柯黑帳號名
						aSpans = child.getElementsByTagName("span");
						for( iSpan = 0 ; iSpan < aSpans.length ; ++iSpan ){
							strKoHeiName = aSpans[iSpan].innerHTML;
							strKoHeiName = strKoHeiName.replace(/^\s*/, '');
							strKoHeiName = strKoHeiName.replace(/\n\s*/, '');
						}
						
						// 解析檢舉影片網址
						strReportUrl = window.location.href;
						strReportUrl = strReportUrl.replace(/^.*\/\/[^\/]+/, '');
						strReportUrl = strReportUrl.replace('/watch?v=', '');
						strReportUrl = strReportUrl.replace(/\&.*/, '');
					}
					else{
						bHasBtn = true; // 不新增按鈕
						break;
					}
				}
				else if( child.id === "published-time-text" ){
					if( child.firstChild ){
						// 解析被檢舉留言之連結
						strCommentUrl = child.firstChild.href;
						strCommentUrl = strCommentUrl.replace(/^.*\/\/[^\/]+/, '');
					}
				}
				
				child = child.nextSibling;
			}

			if( !bHasBtn ){
				strReportParam = strKoHeiName + ",|" + strKoHeiUrl + ",|" + strReportUrl + ",|" + strCommentUrl + ",|" + g_strUserUrl;
				// 若沒新增過Report按鈕，則新增一個
				if( isReportBtnVisible() ){
					// 若檢舉按鈕可顯示才添加按鈕
					var a = document.createElement("a");
					a.id = "BtnReport";
					a.name = strReportParam;
					a.title = "[" + chrome.i18n.getMessage("report_kohei") + "]";
					var span = document.createElement("span");
					span.setAttribute('style', 'color:red;font-size:13px;margin-left:25px;');
					span.innerHTML = a.title;
					a.appendChild(span);
					a.href = "#";
					a.setAttribute( 'onclick', "onClkBtnReport('" + a.name + "');return false;");
					aClasses[iClass].appendChild(a);
				}
				
				// 添加右鍵選單事件
				aClasses[iClass].name = strReportParam;
				aClasses[iClass].oncontextmenu = function(e){
					var e = e || window.event;
					//滑鼠點的座標
					var oX = e.clientX;
					var oY = e.clientY;
					//選單出現後的位置
					var menu = document.getElementById("KHDT_contextmenu");
					if( menu ){
						menu.style.display = "block";
						menu.style.left = oX + "px";
						menu.style.top = $(window).scrollTop() + oY + "px";
						menu.name = this.name;
					}
					//阻止瀏覽器預設事件
					return false;//一般點選右鍵會出現瀏覽器預設的右鍵選單，寫了這句程式碼就可以阻止該預設事件。
				};
			}
		}
	}
}

// 更新頁面顯示
function updatePageContent(){
	
	if( !isNotKoHei() ) // 若可能為柯黑則要先驗證，不為柯黑則無須驗證
		verifyMyIdentity();
	
	// 標記柯黑
	markKoHei();
	
	// 顯示檢舉按鈕在帳號旁，以及添加右鍵選單
	showReportBtn();
}

// 向頁面注入JS
function injectCustomJs(jsPath){
    jsPath = jsPath || 'js/inject.js';
    var temp = document.createElement('script');
    temp.setAttribute('type', 'text/javascript');
    // 獲得的地址類似：chrome-extension://ihcokhadfjfchaeagdoclpnjdiokfakg/js/inject.js
    temp.src = chrome.extension.getURL(jsPath);
    temp.onload = function(){
        // 放在頁面不好看，執行完後移除掉
        this.parentNode.removeChild(this);
    };
	document.head.appendChild(temp);
	

    var temp = document.createElement('script');
    temp.setAttribute('type', 'text/javascript');
    // 獲得的地址類似：chrome-extension://ihcokhadfjfchaeagdoclpnjdiokfakg/js/inject.js
    temp.src = chrome.extension.getURL('js/jquery-3.0.0.min.js');
    temp.onload = function(){
        // 放在頁面不好看，執行完後移除掉
        this.parentNode.removeChild(this);
	};
	document.head.appendChild(temp);
}

// Listen Mouse Click Event
function injectMouseClkEvent(){
	var aClasses = document.getElementsByClassName("style-scope ytd-watch-flexy");
	for( iClass = 0; iClass < aClasses.length; ++iClass ){
		if( aClasses[iClass].id === "columns" ){
			aClasses[iClass].addEventListener('click', updatePageContent, true);
		}
	}
}

// 向頁面插入使用提示
function injectUsageTip( bShow ){
	var head = document.getElementById('masthead-container');
	var child = head.firstChild;
	while( child ){
		if( child.id === 'usageTip' ){
			if( bShow )
				return; // 頁面已經存在使用提示，則不再添加，避免短時間內刷新頁面導致重複顯示
			else
				break;
		}
		child = child.nextSibling;
	}

	if( bShow ){
		// 若不存在usageTip則添加
		var div = document.createElement("div");
		div.id = "usageTip";
		div.setAttribute('style', 'text-align:center;background-color:white;font-size:14px;');
		div.innerHTML = "<span>柯黑偵測器會自動將柯黑以<\/span><span style=\"background-color:#000000;color:#ffffff\">黑底白字<\/span><span>標記，方便大家識別。 　　　　　 <\/span><span>網頁捲軸拉到最底部 或 點擊網頁空白處，將會自動重新偵測新內容。 　　　　　 詳細資訊請參考右上角[柯黑偵測器]擴充功能按鈕。 　　　　<\/span><a href=\"#\" onclick=\"onClkBtnCloseUsageTip();return false;\">不再提示</a>";
		head.appendChild(div);
	}
	else if( child ){
		head.removeChild(child);
		// 寫入紀錄表示提示不再顯示
		chrome.storage.local.set({showUsageTip: 0}, function(){});
	}
}

// 響應讀取紀錄判斷是否向頁面插入使用提示
function onGetStgShowUsageTip( data ){
	if( typeof( data.showUsageTip ) === "undefined" )
		injectUsageTip( true );
	else
		injectUsageTip( data.showUsageTip );
}
// 自動作答
function loadAnsBtn() {
	
	var headings = document.getElementsByClassName('page-header-headings')[0];
	var child = headings.firstChild;
	while (child) {
		if (child.className==="tooltiptext")
			break;
		child = child.nextSibling;
	}
	if (child) 
		return;

	var span = document.createElement("span");
	span.classList.add("tooltiptext");
	// span.innerHTML = chrome.i18n.getMessage("setting_options");
	
	var a = document.createElement("a");
	a.id = "BtnSettingOptions";
	a.href = "#";
	a.setAttribute( 'onclick', "onClkAutomatedAns();return false;");
	a.innerText = "自動回答";

	span.appendChild(a);
	headings.appendChild(span);
}

// 向頁面插入"偵測器設定"按鈕
function injectOptionsPageBtn(){
	var divEnd = document.getElementById('end');
	var child = divEnd.firstChild;
	while( child ){
		if( child.id === 'buttons' )
			break; // 找到按鈕列

		child = child.nextSibling;
	}
	if( !child )
		return;
	
	// 檢索按鈕列中是否有"偵測器設定"按鈕
	var divBtns = child;
	child = divBtns.firstChild;
	while( child ){
		if( child.id === 'DivSettingOptions' ){
			return; // 頁面已經存在"偵測器設定"按鈕，則不再添加，避免短時間內刷新頁面導致重複顯示
		}
		child = child.nextSibling;
	}
	
	// 添加"偵測器設定"按鈕
	var divBtn = document.createElement("div");
	divBtn.id = "DivSettingOptions";
	
	var a = document.createElement("a");
	a.id = "BtnSettingOptions";
	a.href = "#";
	a.setAttribute( 'onclick', "onClkSettingOptions();return false;");
	
	var span = document.createElement("span");
	span.classList.add("tooltiptext");
	span.innerHTML = chrome.i18n.getMessage("setting_options");
	a.appendChild(span);
	
	divBtn.appendChild(a);
	divBtns.insertBefore(divBtn, divBtns.childNodes[0]);
	
}

// 向頁面插入自訂右鍵選單
function injectContextMenu(){
	// 持續檢測評論區是否已載入
	var aElements;
	var iTimes = 0;
	var idx = 0;
	for( iTimes = 0 ; iTimes < 100 ; ++iTimes ){ // 限制重新檢測100次
		aElements = document.getElementsByClassName("style-scope ytd-item-section-renderer");

		for( idx = 0; idx < aElements.length; ++idx ){
			if(aElements[idx].id === "contents"){
				break; // 評論區已載入
			}
		}
		
		if( idx < aElements.length )
			break; // 結束檢測評論區
	}
	
	if( iTimes >= 100 )
		return;
	
	var menu = document.getElementById("KHDT_contextmenu");
	if( !menu ){
		menu = document.createElement("ul");
		menu.id = "KHDT_contextmenu";
		menu_child = document.createElement("li");
		menu_child.id = "KHDT_contextmenu_report";
		menu_child.innerHTML = chrome.i18n.getMessage("report_kohei");
		menu.appendChild(menu_child);
		menu_child.addEventListener('click', function(){ // 點擊Report按鈕事件
			window.postMessage({cmd:'report', value:menu_child.parentNode.name}, '*');
		});
		document.body.appendChild(menu);
	}
}
