﻿var g_strUID;

/*
chrome.runtime.onInstalled.addListener(function(){
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function(){
        chrome.declarativeContent.onPageChanged.addRules([
            {
                conditions: [
					// 只有打開YOUTUBE才顯示pageAction
                    new chrome.declarativeContent.PageStateMatcher({pageUrl: {urlContains: 'youtube.com'}})
                ],
                actions: [new chrome.declarativeContent.ShowPageAction()]
            }
        ]);
    });
});
*/

// Get UID From storage.local
chrome.storage.local.get('koheidetector_uid', getUID);

// sendMessageToContentScript
function sendMessageToContentScript(message, callback){
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs)
    {
		if( typeof(tabs[0]) !== "undefined" ){
			chrome.tabs.sendMessage(tabs[0].id, message, function(response)
			{
				if(callback) callback(response);
			});
		}
    });
}

// Sleep
function sleep(ms) {
	var start = new Date().getTime(); 
	while(1)
		 if ((new Date().getTime() - start) > ms)
			  break;
}

// To handle youtube video page
chrome.webNavigation.onHistoryStateUpdated.addListener(function(details){
    if(details.frameId === 0){
        // Fires only when details.url === currentTab.url
        chrome.tabs.get(details.tabId, function(tab){
            if(tab.url === details.url){ // Reload Page
				//console.log("onHistoryStateUpdated");
				sleep(100);
				sendMessageToContentScript({cmd:'reload_page', value:'reload page'}, function(response){
					//console.log(response);
				});
            }
        });
    }
});

// Message Listener
chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    if(request.cmd === 'report'){ // Report Kohei
		sendResponse('[ KoHeiDetector ] background::Message Listener - Report KoHei');
		var aData = request.value.split(",|");
		var aParams = { kohei_name:aData[0], kohei_url:aData[1], report_url:aData[2], comment_url:aData[3], reporter_url:aData[4], reporter_uid:g_strUID };
		post( "http://hellokohei.unaux.com/report.php", aParams );
	}
	else if(request.cmd === 'delay_init'){ // Delay Initial
		sleep(2000);
		sendMessageToContentScript({cmd:'delay_init', value:'delay init'}, function(response){
			//console.log(response);
		});
	}
});

// POST Report
function post(URL, params) {
	var form = document.createElement("form");
	form.id = "report";
	form.action = URL;
	form.method = "post";
	form.setAttribute("target", "_blank");
	form.style.display = "none";
	for(var tag in params){
		var opt = document.createElement("textarea");
		opt.name = tag;
		opt.value = params[tag];
		form.appendChild(opt);
	}
	document.body.appendChild(form);
	form.submit();
	
	// 避免元素持續增長，執行完後移除掉
    document.body.removeChild(form);
}

function guid() {
  return "ss-s-s-s-sss".replace(/s/g, s4);
}

function s4() {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
}

function getUID(data){
	g_strUID = data.koheidetector_uid;
	
	if( typeof(g_strUID) === "undefined" ){
		g_strUID = guid();
		
		chrome.storage.local.set({'koheidetector_uid': g_strUID}, function(){
			console.log("[ KoHeiDetector ] background::getUID - Success!");
		});
	}
}
