// 點擊Report按鈕事件
function onClkBtnReport( post ){
	// 發送消息給content-script.js to POST Report
	window.postMessage({cmd:'report', value:post}, '*');
}

// 點擊"不再提示"使用提示按鈕
function onClkBtnCloseUsageTip(){
	// 發送消息給content-script.js to Close Usage Tip
	window.postMessage({cmd:'close_usage_tip', value:''}, '*');
}

// 點擊"偵測器設定"按鈕事件
function onClkSettingOptions(){
	// 發送消息給content-script.js to 偵測器設定
	window.postMessage({cmd:'setting_options', value:''}, '*');
}

// 點擊"自動回答"按鈕事件
function onClkAutomatedAns(){
	// 發送消息給content-script.js to 自動回答
	window.postMessage({cmd:'automated_answer', value:''}, '*');
}