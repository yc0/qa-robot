var g_bShowReportBtn;
var g_aMarkStyle;

//
document.addEventListener('DOMContentLoaded', function(){
	var btnSave = document.getElementById('BtnSave');
	btnSave.addEventListener('click', function(){
		onClkSaveBtn();
    });
	
	// 從chrome.storage.sync獲取 [顯示"檢舉此用戶為疑似柯黑"按鈕] 選項的狀態並設置
	//chrome.storage.sync.clear(function(){});
	chrome.storage.sync.get('khdt_opt_showReportBtn', getStgSync_Option_ShowReportBtn);
	
	// 從chrome.storage.sync獲取 [柯黑標記樣式] 選項的狀態並設置
	chrome.storage.sync.get('khdt_opt_markStyle', getStgSync_Option_MarkStyle);
});

//
function getStgSync_Option_ShowReportBtn(data){
	g_bShowReportBtn = data.khdt_opt_showReportBtn;

	if( typeof( g_bShowReportBtn ) === "undefined" ){
		g_bShowReportBtn = true;
		chrome.storage.sync.set({khdt_opt_showReportBtn: g_bShowReportBtn}, function(){});
	}
	
	if( g_bShowReportBtn )
		document.getElementById("showReportBtn").checked = true;
	else
		document.getElementById("hideReportBtn").checked = true;
}

//
function getStgSync_Option_MarkStyle(data){
	g_aMarkStyle = data.khdt_opt_markStyle;
	
	if( typeof( g_aMarkStyle ) === "undefined" ){
		g_aMarkStyle = [ "#000000", "#ffffff" ];
		chrome.storage.sync.set({khdt_opt_markStyle: g_aMarkStyle}, function(){});
	}

	if( g_aMarkStyle[0] === "#000000" && g_aMarkStyle[1] === "#ffffff" )
		document.getElementById("markstyle_black_white").checked = true;
	else if( g_aMarkStyle[0] === "#ffffff" && g_aMarkStyle[1] === "#000000" )
		document.getElementById("markstyle_white_black").checked = true;
	else if( g_aMarkStyle[0] === "#7f7f7f" && g_aMarkStyle[1] === "#ffffff" )
		document.getElementById("markstyle_grey_white").checked = true;
}

// 點擊儲存設定按鈕
function onClkSaveBtn(){
	// 儲存 [顯示"檢舉此用戶為疑似柯黑"按鈕] 選項的狀態
	g_bShowReportBtn = document.getElementById("showReportBtn").checked;
	chrome.storage.sync.set({khdt_opt_showReportBtn: g_bShowReportBtn}, function(){});
	
	// 儲存 [柯黑標記樣式] 選項的狀態
	if( document.getElementById("markstyle_black_white").checked ){
		g_aMarkStyle[0] = "#000000";
		g_aMarkStyle[1] = "#ffffff";
	}
	else if( document.getElementById("markstyle_white_black").checked ){
		g_aMarkStyle[0] = "#ffffff";
		g_aMarkStyle[1] = "#000000";
	}
	else if( document.getElementById("markstyle_grey_white").checked ){
		g_aMarkStyle[0] = "#7f7f7f";
		g_aMarkStyle[1] = "#ffffff";
	}

	chrome.storage.sync.set({khdt_opt_markStyle: g_aMarkStyle}, function(){});
}
